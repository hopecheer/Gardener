﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

namespace Gardener.Enums
{
    /// <summary>
    /// 请求登录的客户端类型
    /// </summary>
    public enum LoginClientType
    {
        /// <summary>
        /// 浏览器类型
        /// </summary>
        Browser,

        /// <summary>
        /// 桌面客户端
        /// </summary>
        Desktop,

        /// <summary>
        /// 手机客户端
        /// </summary>
        Mobile
    }
}
