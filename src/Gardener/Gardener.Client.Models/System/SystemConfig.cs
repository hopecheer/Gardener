﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

namespace Gardener.Client.Models
{
    public class SystemConfig
    {
        /// <summary>
        /// Copyright内容
        /// </summary>
        public string Copyright { get; set; }

        public string SystemName { get; set; }

        public string SystemDescription { get; set; }
    }
}
