﻿// -----------------------------------------------------------------------------
// 园丁,是个很简单的管理系统
//  gitee:https://gitee.com/hgflydream/Gardener 
//  issues:https://gitee.com/hgflydream/Gardener/issues 
// -----------------------------------------------------------------------------

namespace Gardener.Application.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class AuditEntitySearchInput : PagedSearchBaseInfo<AuditEntityDto>
    {
    }
}
